#ifndef TEST_CECIL_H
#define TEST_CECIL_H
#include "eif_eiffel.h"

void set_eiffel_application(EIF_OBJECT);

void clear_eiffel_application(void);

void call_method_print_float(void);

#endif /* TEST_CECIL_H */
