#include "test_cecil.h"

EIF_OBJECT eiffel_application;

void set_eiffel_application(EIF_OBJECT a_object){
	eiffel_application = eif_adopt(a_object);
}

void clear_eiffel_application(void){
	eif_wean(eiffel_application);
	eiffel_application = NULL;
}

void call_method_print_float(void){
	float lTest = 0.25;
	EIF_PROCEDURE lEifProc;
	EIF_TYPE_ID lEifTypeId;
	lEifTypeId = eif_type_id ("APPLICATION");
	lEifProc = eif_procedure ("method_print_float", lEifTypeId);
	(lEifProc) (eif_access(eiffel_application), lTest);
}

