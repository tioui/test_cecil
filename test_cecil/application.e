class APPLICATION create make feature

	make
		do
			set_eiffel_application(Current)
			call_method_print_float
			clear_eiffel_application
		end

	set_eiffel_application(a_object_a:ANY)
		external
			"C use <test_cecil.h>"
		end

	clear_eiffel_application
		external
			"C use <test_cecil.h>"
		end

	call_method_print_float
		external
			"C use <test_cecil.h>"
		end

	method_print_float(a_float:REAL_32)
		do
			print("Float: " + a_float.out + "%N")
		end

end
